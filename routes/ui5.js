var express = require('express');
var router = express.Router();

router.get('/index.html', function(req, res, next) {
  req.session.cvl = "In UI5 session";
  res.render('ui5', { });
});

module.exports = router;
